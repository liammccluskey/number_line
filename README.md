#number_line

The number_line python package allows users to create a number line (as an object) to which they can add or remove points and ranges, and perform many other functions. The sections below explain how to use this package.

---
##Installing this Package

Since this package is available on the Python Package Index, you can install it by running the command shown below.
```
pip install number_line
```

Alternatively, you can install this package by cloning this repositiory. The method above, however, is preferred. 

---
##Importing this Package

Once you have installed this package, you can import it. In order to import this package for use within a file, type the code shown below.
```python
from number_line import NumberLine
```

---
##Creating a NumberLine Object

Once you have imported this package, you can create a NumberLine object. In order to do this, type the code show below.
```python
nl = NumberLine()
```
Since objects of type NumberLine do not take any input parameters, you should not pass any arguments when instantiating the class.

---
##Adding a Point to the Number Line

**Method to Use: ** add_point(pointVal)

**Return Type: ** None

**Arguments (listed in the order in which they should be passed in): **

1. pointVal (Type: Numeric): This argument is the value of the point you want to add to the number line.

Once you have created a NumberLine object, you can add a point to it. When adding a point to the number line, you must use the add_point() method and pass in the argument listed above. In order to do this for sample points of 1 and 2, type the code shown below.
```python
nl.add_point(1)
nl.add_point(2)
```
The code above will add points at 1 and 2 to the NumberLine object you created in the section above.

---
##Removing a Point from the Number Line

**Method to Use: ** remove_point(pointVal)

**Return Type: ** None

**Arguments (listed in the order in which they should be passed in): **

1. pointVal (Type: Numeric): This argument is the value of the point you want to remove from the number line.

When removing a point from the number line, you must use the remove_point() method and pass in the argument listed above. In order to remove a point from the number line, type the code shown below.
```python
nl.remove_point(1)
nl.remove_point(2)
```
The code above will remove points at 1 and 2 from the number line. If you try to remove a point from the number line that is not contained on the numberline, you will receive an error message.

---
##Adding a Range to the Number Line

**Method to Use: ** add_range(lowerVal, includesLowerVal, upperVal, includesUpperVal)

**Return Type: ** None

**Arguments (listed in the order in which they should be passed in): **

1. lowerVal (Type: Numeric): This argument is the value of the lower end of the range you want to add.
2. inlcudesLowerVal: (Type: boolean): This argument is True if you want to include the value at the lower end of the range, and False otherwise.
3. upperVal (Type: Numeric): This argument is the value of the upper end of the range you want to add.
4. includesUpperVal (Type: boolean): This argument is True if you want to include the value at the upper end of the range, and False otherwise.

When adding a range to the number line, you must use the add_range() method and pass in the four arguments listed above in that order. In order to do this for a sample range (0,10], type the code shown below. 
```python
nl.add_range(0,False,10,True)
```
The code above will add the sample range (0,10] to the number line. If this range overlaps with another range, the number line will automatically  merge the ranges that overlap. 

---
##Removing a Range from the Number Line

**Method to Use: ** remove_range(lowerVal, includesLowerVal, upperVal, includesUpperVal)

**Return Type: ** None

**Arguments (listed in the order in which they should be passed in): **

1. lowerVal (Type: Numeric): This argument is the value of the lower end of the range you want to remove.
2. inlcudesLowerVal: (Type: boolean): This argument is True if you want to include the value at the lower end of the range, and False otherwise.
3. upperVal (Type: Numeric): This argument is the value of the upper end of the range you want to remove.
4. includesUpperVal (Type: boolean): This argument is True if you want to include the value at the upper end of the range, and False otherwise.

When removing a range from the number line, you must use the remove_range() method and pass in the arguments listed above in that order. In order to remove a sample range of (0,5], type the code shown below.
```python
nl.remove_range(0,False,5,True)
```
The code above will remove the sample range of (0,5] from the number line. If you try to remove a range that contains values that are not currently contained on the number line, you will receive an error message.

---
##Checking if the Number Line Contains a Point

**Method to Use: ** contains_point(pointVal)

**Return Type: ** boolean

**Arguments (listed in the order in which they should be passed in): **

1. pointVal (Type: Numeric): This argument is the value of the point you want to check whether the number line contains.


When checking whether the number line currently contains a point, you must use the contains_point() method and pass in the argument listed above. In order to check if the number line contains the point 10, type the code shown below.
```python
nl.contains_point(10)
```
The code above will return True if the number line contains the sample point 10, and False if it does not contain that point. 

---
##Checking if the Number Line Completely Contains a Range 

**Method to Use: ** contains_range_totally(lowerVal, includesLowerVal, upperVal, includesUpperVal)

**Return Type: ** boolean

**Arguments (listed in the order in which they should be passed in): **

1. lowerVal (Type: Numeric): This argument is the value of the lower end of the range you want to check whether the number line contains.
2. inlcudesLowerVal: (Type: boolean): This argument is True if you want to include the value at the lower end of the range, and False otherwise.
3. upperVal (Type: Numeric): This argument is the value of the upper end of the range you want to check whether the number line contains.
4. includesUpperVal (Type: boolean): This argument is True if you want to include the value at the upper end of the range, and False otherwise .

When checking whether the number line currently completely contains a range, you must use the contains_range_totally() method and pass in the arguments listed above in that order. In order to do this for a sample range of (0,10), type the code shown below.
```python
nl.contains_range_totally(0,False,10,False)
```
The code above will return True if the sample range (0,10) is completely contained in the number line, and False if it is not. 

---
##Printing a Visual Representation of the Number Line

**Method to Use: ** print_number_line()

**Return Type: ** None

**Arguments (listed in the order in which they should be passed in): ** None

When printing a visual representation of the number line, you must use the print_number_line() method and pass in no arguments. In order to do this, type the code shown below. 
```python
nl.print_number_line()
```
For a number line that contains the points 1,2,3,4 and the ranges (0.5,0.6) and (4.5, 5.5], the code above will print the following to the command window.
```python
__(0.5,0.6)__[1,1]__[2,2]__[3,3]__[4,4]__(4.5,5.5]__
```

---


